

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity TOP_tb is

end TOP_tb;

ARCHITECTURE bench OF TOP_tb IS

  COMPONENT ASCENSOR
      PORT(
  	clk,P1,P2,P3,P4: IN STD_LOGIC;
  	motor: OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
  	puerta: OUT STD_LOGIC;
  	DISPLAY1, DISPLAY2: OUT STD_LOGIC_VECTOR (6 DOWNTO 0)
  );
        END COMPONENT;


SIGNAL bo1: STD_LOGIC:='0';
SIGNAL bo2: STD_LOGIC:='0';
SIGNAL bo3: STD_LOGIC:='0';
SIGNAL bo4: STD_LOGIC:='0';
SIGNAL clock: STD_LOGIC := '0';
SIGNAL mot: STD_LOGIC_VECTOR (1 DOWNTO 0);
SIGNAL pue: STD_LOGIC;
SIGNAL DIS1, DIS2: STD_LOGIC_VECTOR (6 DOWNTO 0);

CONSTANT k: time := 100 ns; 

BEGIN 
uut: ASCENSOR 
PORT MAP ( 
clk => clock, 
P1 => bo1,
P2 => bo2,
P3 => bo3,
P4 => bo4,
motor => mot,
puerta => pue,
DISPLAY1 => DIS1,
DISPLAY2 => DIS2
); 

clock <= not clock after 0.5 * k; 
bo1 <= '1' after 0.25 * k,'0' after 0.5 * k;


END ARCHITECTURE;

