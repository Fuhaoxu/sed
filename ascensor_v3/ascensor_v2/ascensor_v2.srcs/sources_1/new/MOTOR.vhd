
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity MOTOR is
    Port ( clock : in STD_LOGIC;
           SENSOR_MOTOR : in STD_LOGIC_VECTOR (1 downto 0);
           LED1 : out STD_LOGIC;
           LED2 : out STD_LOGIC);
end MOTOR;

architecture Behavioral of MOTOR is

begin

motorrr: PROCESS (clock, SENSOR_MOTOR)
		BEGIN
    IF rising_edge (clock) THEN
          LED1 <= '0' WHEN SENSOR_MOTOR = "00" ELSE
				 '0' WHEN SENSOR_MOTOR = "01" ELSE
				'1' WHEN SENSOR_MOTOR = "10" ;
		  LED2 <= '0' WHEN SENSOR_MOTOR = "00" ELSE
				 '1' WHEN SENSOR_MOTOR = "01" ELSE
				'0' WHEN SENSOR_MOTOR = "10" ;
	END IF;
END PROCESS;

end Behavioral;
