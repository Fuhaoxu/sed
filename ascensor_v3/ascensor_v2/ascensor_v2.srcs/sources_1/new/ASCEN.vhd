
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity ASCEN is
    Port ( clock : in STD_LOGIC;
           enable : in STD_LOGIC;
           planta_destino : in STD_LOGIC_VECTOR (2 DOWNTO 0);
           motor_out : out STD_LOGIC_VECTOR (1 downto 0);
           planta_act_sal : out STD_LOGIC_VECTOR (2 DOWNTO 0);
           puerta_ordenn : out STD_LOGIC);
end ASCEN;

architecture Behavioral of ASCEN is
    TYPE state_type IS (S1, S2, S3);
    SIGNAL state, next_state: state_type;
    SIGNAL P_DEST: STD_LOGIC_VECTOR (2 DOWNTO 0);
    SIGNAL PUERTA_ORDEN: STD_LOGIC;
    SIGNAL ESTADO_PUERTA: STD_LOGIC;
    SIGNAL ESTADO_MOTOR: STD_LOGIC_VECTOR (1 DOWNTO 0);

    
    SIGNAL P_D: INTEGER;
    SIGNAL P_A: INTEGER:=1;
		
begin     --------------------------------------------------------------------------------------------

--		P_A <= to_integer(unsigned(planta_act_sal));  -- UNSIGNED ES OBLIGATORIO Y NO SE PREGUNTA POR QUE
		P_D <= to_integer(unsigned(planta_destino));  -- UNSIGNED ES OBLIGATORIO Y NO SE PREGUNTA POR QUE


SINCRONIZACION_RELOJ: PROCESS (clock)
	BEGIN
		IF rising_edge (clock) THEN
	state <= next_state;
END IF;
END PROCESS;


NEXT_STATE_DECODE: PROCESS ---------(PLANTA_ACTUAL, ESTADO_PUERTA, P_DEST)
  BEGIN
		next_state <= S1;
		

	IF (state=S1) THEN
IF (P_D-P_A=1 AND enable = '0') THEN
				next_state <= S2;
			    WAIT FOR 3ns;
				P_A <= P_D;
				next_state <= S1;
				PUERTA_ORDEN <= '1';
ELSIF (P_D-P_A=2 AND enable = '0') THEN
				next_state <= S2;
				WAIT FOR 3ns;
				P_A <= P_A+1;
				WAIT FOR 1ns;
				P_A <= P_D;
				next_state <= S1;
				PUERTA_ORDEN <= '1';
ELSIF (P_D-P_A=3 AND enable = '0') THEN
				next_state <= S2;
				WAIT FOR 3ns;
				P_A <= P_A+1;
				WAIT FOR 1ns;
				P_A <= P_A+1;
                WAIT FOR 1ns;
                P_A <= P_D;
				next_state <= S1;
				PUERTA_ORDEN <= '1';
ELSIF (P_A-P_D=1 AND enable = '0') THEN
				next_state <= S3;
			    WAIT FOR 3ns;
				P_A <= P_D;
				next_state <= S1;
				PUERTA_ORDEN <= '1';
ELSIF (P_A-P_D=2 AND enable = '0') THEN
				next_state <= S3;
				WAIT FOR 3ns;
				P_A <= P_A-1;
				WAIT FOR 1ns;
                P_A <= P_D;
				next_state <= S1;
				PUERTA_ORDEN <= '1';
ELSIF (P_A-P_D=3 AND enable = '0') THEN
				next_state <= S3;
				WAIT FOR 3ns;
				P_A <= P_A-1;
				WAIT FOR 1ns;
				P_A <= P_A-1;
                WAIT FOR 1ns;
                P_A <= P_D;
				next_state <= S1;
				PUERTA_ORDEN <= '1';
END IF;
END IF;
END PROCESS;

planta_act_sal <= STD_LOGIC_VECTOR(TO_UNSIGNED(P_A, planta_destino'length));


TRATAMIENTO_PUERTA: PROCESS(clock,P_D)
BEGIN
--CASE (state) IS
--	WHEN S1 =>
    IF state=S1 THEN
	IF rising_edge (clock) THEN
	IF (P_D=1 OR P_D=2 OR P_D=3 OR P_D=4) THEN
    PUERTA_ORDEN <= '0';
END IF; 
END IF;
END IF;
--END CASE;
END PROCESS;

puerta_ordenn <= PUERTA_ORDEN;   ---asignar la senial a la salida, no puedo asignar la salida en los dos process, que sale error de implementacion

Estado_del_motor: PROCESS (state)
	BEGIN
    motor_out <= "00" WHEN state=S1 ELSE
                 "01" WHEN state=S2 ELSE
                 "10" WHEN state=S3 ;
	END PROCESS;

end Behavioral;
