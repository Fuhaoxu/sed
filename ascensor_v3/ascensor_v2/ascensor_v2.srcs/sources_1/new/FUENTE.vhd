
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity TOP is
    Port ( clk : in STD_LOGIC;
           P1 : in STD_LOGIC;
           P2 : in STD_LOGIC;
           P3 : in STD_LOGIC;
           P4 : in STD_LOGIC;
           motor_LED1 : out STD_LOGIC;
           motor_LED2 : out STD_LOGIC;
           puerta : out STD_LOGIC;
           DISPLAY1 : out STD_LOGIC_VECTOR (6 downto 0);
           DISPLAY2 : out STD_LOGIC_VECTOR (6 downto 0));
end TOP;

architecture Behavioral of TOP is

COMPONENT Debouncer
    PORT(
           clock : in STD_LOGIC;
           b1 : in STD_LOGIC;
           b2 : in STD_LOGIC;
           b3 : in STD_LOGIC;
           b4 : in STD_LOGIC;
           P1_OUT : out STD_LOGIC;
           P2_OUT : out STD_LOGIC;
           P3_OUT : out STD_LOGIC;
           P4_OUT : out STD_LOGIC);
END COMPONENT;

COMPONENT DEC1 
	PORT(
	clock, boton1, boton2, boton3, boton4: IN STD_LOGIC;
	planta_destino1: OUT STD_LOGIC_VECTOR (2 DOWNTO 0)
);
		END COMPONENT;
		
COMPONENT DEC2
	PORT(
	clock: IN STD_LOGIC;
	planta_actual2: IN STD_LOGIC_VECTOR (2 DOWNTO 0);
	pines_display2: OUT STD_LOGIC_VECTOR (6 DOWNTO 0)
);
		END COMPONENT;

COMPONENT DEC3
	PORT(
	clock: IN STD_LOGIC;
	planta_destino3: IN STD_LOGIC_VECTOR (2 DOWNTO 0) ;
	pines_display1: OUT STD_LOGIC_VECTOR (6 DOWNTO 0)
);
		END COMPONENT;

COMPONENT PUERTTA 
	PORT(
	clock: IN STD_LOGIC;
	orden: IN STD_LOGIC;
	resultado: OUT STD_LOGIC;
	LED0: OUT STD_LOGIC
);
		END COMPONENT;

COMPONENT MOTOR 
	PORT(
	clock: IN STD_LOGIC;
	SENSOR_MOTOR: IN STD_LOGIC_VECTOR (1 DOWNTO 0);
	LED1: OUT STD_LOGIC;
    LED2: OUT STD_LOGIC
);
		END COMPONENT;

COMPONENT ASCEN
	PORT(
	planta_destino: IN STD_LOGIC_VECTOR (2 DOWNTO 0);
    clock: IN STD_LOGIC;
    enable: IN STD_LOGIC;
    motor_out: OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
    planta_act_sal: OUT STD_LOGIC_VECTOR (2 DOWNTO 0);
    puerta_ordenn: OUT STD_LOGIC
);
		END COMPONENT;


------------------------------------ DECLARACION DE SENIALES
SIGNAL P_DEST: STD_LOGIC_VECTOR (2 DOWNTO 0);
SIGNAL PUERTA_ORDEN: STD_LOGIC;
SIGNAL ESTADO_PUERTA: STD_LOGIC;
SIGNAL ESTADO_MOTOR: STD_LOGIC_VECTOR (1 DOWNTO 0);
SIGNAL PLANTA_ACTUAL: STD_LOGIC_VECTOR (2 DOWNTO 0) := "001";
TYPE state_type IS (S1, S2, S3);
SIGNAL state, next_state: state_type;
SIGNAL P1C, P2C, P3C, P4C: STD_LOGIC;

begin

antirrebote: Debouncer
    PORT MAP(
    clock => clk,
    b1 => P1,
    b2 => P2,
    b3 => P3,
    b4 => P4,
    P1_OUT => P1C,
    P2_OUT => P2C,
    P3_OUT => P3C,
    P4_OUT => P4C
    );

Decoder1: DEC1
	PORT MAP( 
	clock => clk,
	boton1 => P1C,
	boton2 => P2C,
	boton3 => P3C,
    boton4 => P4C,
    planta_destino1 => P_DEST  );

Decoder3: DEC3
	PORT MAP(
	clock => clk,
	planta_destino3 => P_DEST,
	pines_display1 => DISPLAY1
);


Puertaa: PUERTTA 
	PORT MAP(	
	clock => clk,
	orden => PUERTA_ORDEN,
	resultado => ESTADO_PUERTA,
    LED0 => puerta
);


Mottor: MOTOR
	PORT MAP(
	clock => clk,
	SENSOR_MOTOR => ESTADO_MOTOR,
	LED1 => motor_LED1,
	LED2 => motor_LED2
);


Decoder2: DEC2
	PORT MAP(
	clock => clk,
	pines_display2 => DISPLAY2,
	planta_actual2 => PLANTA_ACTUAL
);


Inst_Ascensor: ASCEN PORT MAP(
	clock => clk,
	enable => ESTADO_PUERTA,
	motor_out => ESTADO_MOTOR,
	puerta_ordenn => PUERTA_ORDEN,
	planta_act_sal => PLANTA_ACTUAL,
	planta_destino => P_DEST
);

end Behavioral;
