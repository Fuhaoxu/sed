


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DEC2 is
    Port ( clock : in STD_LOGIC;
           planta_actual2 : in STD_LOGIC_VECTOR (2 DOWNTO 0);
           pines_display2 : out STD_LOGIC_VECTOR(6 DOWNTO 0));
end DEC2;

architecture Behavioral of DEC2 is

begin

DECODIFICADOR2: PROCESS (clock, planta_actual2)
	BEGIN
		IF rising_edge (clock) THEN
    pines_display2 <= "0110000" WHEN planta_actual2 = "001" ELSE
				"1101101" WHEN planta_actual2 = "010" ELSE
				"1111000" WHEN planta_actual2 = "011" ELSE
				"0110011" WHEN planta_actual2 = "100";
			END IF;
      END PROCESS;

end Behavioral;
