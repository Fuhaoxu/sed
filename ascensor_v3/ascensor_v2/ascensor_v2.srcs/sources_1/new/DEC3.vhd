


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DEC3 is
    Port ( clock : in STD_LOGIC;
           planta_destino3 : in STD_LOGIC_VECTOR (2 DOWNTO 0);
           pines_display1 : out STD_LOGIC_VECTOR (6 downto 0));
end DEC3;

architecture Behavioral of DEC3 is

begin

DECODIFICADOR3: PROCESS (clock, planta_destino3)
		BEGIN
		   IF rising_edge (clock) THEN
				pines_display1 <= "0110000" WHEN planta_destino3 = "001" ELSE
								  "1101101" WHEN planta_destino3 = "010" ELSE
							  	  "1111000" WHEN planta_destino3 = "011" ELSE
								  "0110011" WHEN planta_destino3 = "100" ;
			END IF;       END PROCESS;

end Behavioral;
