
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity PUERTTA is
    Port ( 
           clock: in STD_LOGIC;
           orden : in STD_LOGIC;
           resultado : out STD_LOGIC;
           LED0 : out STD_LOGIC);
end PUERTTA;

architecture Behavioral of PUERTTA is
        SIGNAL led: STD_LOGIC;
        SIGNAL res: STD_LOGIC;
begin

puerrta: PROCESS (clock, orden)
BEGIN
IF rising_edge (clock) THEN
		IF orden = '0' THEN
            resultado <= '1';
           LED0 <= '1';     
     --   ELSIF orden = '0' THEN
      --      resultado <= '0';
       --     LED0 <= '0';      
     END IF;
        END IF;   
END PROCESS;

        
end Behavioral;