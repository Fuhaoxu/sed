
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity Display_refresh is
    Port ( clock : in STD_LOGIC;
           display_selection : out STD_LOGIC_VECTOR (7 downto 0);
           display_number : out STD_LOGIC_VECTOR (6 downto 0);
           planta_actuall : in STD_LOGIC_VECTOR (6 downto 0);
           planta_destinoo : in STD_LOGIC_VECTOR (6 downto 0));
end Display_refresh;

architecture Behavioral of display_refresh is

begin

PROCESS (clock, planta_destinoo, planta_actuall)
VARIABLE cnt: integer  range 0 to 1 :=0;
BEGIN
IF clock'event and clock='1' THEN 
    IF cnt=1 THEN
        cnt:=0;
    ELSE 
    cnt:=cnt+1;
   END IF;
 END IF;
 
 CASE cnt is 
 
        WHEN 0 => display_selection <="11111101";
                  display_number <= planta_destinoo;
 
        WHEN 1 => display_selection <= "11111110";
                  display_number <= planta_actuall;
                  
                
   END CASE;
   END PROCESS;
   
end Behavioral;