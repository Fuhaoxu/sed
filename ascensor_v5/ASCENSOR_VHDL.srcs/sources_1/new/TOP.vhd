
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity TOP is
    Port ( clk : in STD_LOGIC;
           PD1 : in STD_LOGIC;
           PD2 : in STD_LOGIC;
           PD3 : in STD_LOGIC;
           PD4 : in STD_LOGIC;
           PA1 : in STD_LOGIC;
           PA2 : in STD_LOGIC;
           PA3 : in STD_LOGIC;
           PA4 : in STD_LOGIC;
           reset : in STD_LOGIC;
           motor_LED1 : out STD_LOGIC;
           motor_LED2 : out STD_LOGIC;
           LED_INDICADOR_LLEGADDA: out STD_LOGIC;
           puerta : out STD_LOGIC;
           DISPLAY_SELEC : out STD_LOGIC_VECTOR (7 downto 0);
           DISPLAY_NUMER : out STD_LOGIC_VECTOR (6 downto 0));
end TOP;

architecture Behavioral of TOP is

COMPONENT clock_div_1Hz
    PORT(
        clock: IN STD_LOGIC;
        clock_200Hz: OUT STD_LOGIC);
    END COMPONENT;


COMPONENT Debouncer
    PORT(
           clock : in STD_LOGIC;
           b1 : in STD_LOGIC;
           b2 : in STD_LOGIC;
           b3 : in STD_LOGIC;
           b4 : in STD_LOGIC;
           P1_OUT : out STD_LOGIC;
           P2_OUT : out STD_LOGIC;
           P3_OUT : out STD_LOGIC;
           P4_OUT : out STD_LOGIC);
END COMPONENT;

COMPONENT DEC1 
	PORT(
	clock, boton1, boton2, boton3, boton4: IN STD_LOGIC;
	planta_destino1: OUT STD_LOGIC_VECTOR (2 DOWNTO 0)
);
		END COMPONENT;
		
COMPONENT DEC2
	PORT(
	clock: IN STD_LOGIC;
	planta_actual2: IN STD_LOGIC_VECTOR (2 DOWNTO 0);
	pines_display2: OUT STD_LOGIC_VECTOR (6 DOWNTO 0)
);
		END COMPONENT;

COMPONENT DEC3
	PORT(
	clock: IN STD_LOGIC;
	planta_destino3: IN STD_LOGIC_VECTOR (2 DOWNTO 0) ;
	pines_display1: OUT STD_LOGIC_VECTOR (6 DOWNTO 0)
);
		END COMPONENT;


COMPONENT MOTOR 
	PORT(
	clock: IN STD_LOGIC;
	SENSOR_MOTOR: IN STD_LOGIC_VECTOR (1 DOWNTO 0);
	LED1: OUT STD_LOGIC;
    LED2: OUT STD_LOGIC
);
		END COMPONENT;


COMPONENT ASCEN
	PORT(
	       clock : in STD_LOGIC;
           RST: in STD_LOGIC;
           planta_destino : in STD_LOGIC_VECTOR (2 DOWNTO 0);
           planta_actual : in STD_LOGIC_VECTOR (2 DOWNTO 0);
           DIS_DEST: out STD_LOGIC_VECTOR (2 DOWNTO 0);
           DIS_ACT: out STD_LOGIC_VECTOR (2 DOWNTO 0);
           LED_P1:out STD_LOGIC;
           LED0: out STD_LOGIC;
           motor_out : out STD_LOGIC_VECTOR (1 downto 0)
);
		END COMPONENT;
		
		
COMPONENT Display_refresh 
    Port ( clock : in STD_LOGIC;
           display_selection : out STD_LOGIC_VECTOR (7 downto 0);
           display_number : out STD_LOGIC_VECTOR (6 downto 0);
           planta_actuall : in STD_LOGIC_VECTOR (6 downto 0);
           planta_destinoo : in STD_LOGIC_VECTOR (6 downto 0));
END COMPONENT;
		

------------------------------------ DECLARACION DE SENIALES
SIGNAL P_DEST: STD_LOGIC_VECTOR (2 DOWNTO 0);
 SIGNAL PUERTA_ORDEN: STD_LOGIC;
 SIGNAL ESTADO_PUERTA: STD_LOGIC;
SIGNAL ESTADO_MOTOR: STD_LOGIC_VECTOR (1 DOWNTO 0);
SIGNAL PLANTA_ACTUAL: STD_LOGIC_VECTOR (2 DOWNTO 0);
TYPE state_type IS (S1, S2, S3);
SIGNAL state, next_state: state_type;
SIGNAL P1C, P2C, P3C, P4C: STD_LOGIC;    --SENIALES DE 4 BOTNONES DE PLANTA DESTNO
SIGNAL P1A, P2A, P3A, P4A: STD_LOGIC;    --SENIALES DE 4 BOTNONES DE PLANTA ACTUAL
SIGNAL DISPLAY1, DISPLAY2: STD_LOGIC_VECTOR (6 DOWNTO 0);
SIGNAL RELOJ_200Hz: STD_LOGIC;
SIGNAL RESET_SINREBOTE: STD_LOGIC;
SIGNAL PLANTA_ACTUAL_SALEDE_ASCENSOR: STD_LOGIC_VECTOR (2 DOWNTO 0);
SIGNAL PLANTA_DESTINO_SALEDE_ASCENSOR: STD_LOGIC_VECTOR (2 DOWNTO 0);

begin

antirrebote_PD: Debouncer
    PORT MAP(
    clock => clk,
    b1 => PD1,
    b2 => PD2,
    b3 => PD3,
    b4 => PD4,
    P1_OUT => P1C,
    P2_OUT => P2C,
    P3_OUT => P3C,
    P4_OUT => P4C
    );

antirrebote_AD: Debouncer
    PORT MAP(
    clock => clk,
    b1 => PA1,
    b2 => PA2,
    b3 => PA3,
    b4 => PA4,
    P1_OUT => P1A,
    P2_OUT => P2A,
    P3_OUT => P3A,
    P4_OUT => P4A
    );

C_D_1Hz: clock_div_1Hz
    PORT MAP(
        clock => clk,
        clock_200Hz => RELOJ_200Hz
        );

Decoder1: DEC1
	PORT MAP( 
	clock => clk,
	boton1 => P1C,
	boton2 => P2C,
	boton3 => P3C,
    boton4 => P4C,
    planta_destino1 => P_DEST  );
    
  Decoder4: DEC1
	PORT MAP( 
	clock => clk,
	boton1 => P1A,
	boton2 => P2A,
	boton3 => P3A,
    boton4 => P4A,
    planta_destino1 => PLANTA_ACTUAL  );

Decoder3: DEC3
	PORT MAP(
	clock => clk,
	planta_destino3 => PLANTA_DESTINO_SALEDE_ASCENSOR,
	pines_display1 => DISPLAY1
);


Mottor: MOTOR
	PORT MAP(
	clock => clk,
	SENSOR_MOTOR => ESTADO_MOTOR,
	LED1 => motor_LED1,
	LED2 => motor_LED2
);

D_R: Display_refresh
        PORT MAP(
        clock => RELOJ_200Hz,
        display_selection => DISPLAY_SELEC,
        display_number => DISPLAY_NUMER,
        planta_actuall => DISPLAY2,
        planta_destinoo => DISPLAY1
);

Decoder2: DEC2
	PORT MAP(
	clock => clk,
	pines_display2 => DISPLAY2,
	planta_actual2 => PLANTA_ACTUAL_SALEDE_ASCENSOR
);


Inst_Ascensor: ASCEN 
    PORT MAP(
	clock => clk,
	RST => reset,
	motor_out => ESTADO_MOTOR,
	planta_actual => PLANTA_ACTUAL,
	planta_destino => P_DEST,
	DIS_DEST => PLANTA_DESTINO_SALEDE_ASCENSOR,
	DIS_ACT => PLANTA_ACTUAL_SALEDE_ASCENSOR,
	LED_P1 => LED_INDICADOR_LLEGADDA,
	LED0 => puerta
);

end Behavioral;