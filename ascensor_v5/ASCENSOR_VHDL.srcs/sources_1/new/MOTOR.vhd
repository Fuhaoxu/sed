
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity MOTOR is
    Port ( clock : in STD_LOGIC;
           SENSOR_MOTOR : in STD_LOGIC_VECTOR (1 downto 0);
           LED1 : out STD_LOGIC;
           LED2 : out STD_LOGIC);
end MOTOR;

architecture Behavioral of MOTOR is

begin

	LED1 <= SENSOR_MOTOR(1)	;
	LED2 <= SENSOR_MOTOR(0)	;
		
end Behavioral;
