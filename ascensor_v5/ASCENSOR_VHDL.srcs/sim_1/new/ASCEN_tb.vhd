
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity ASCEN_tb is

end ASCEN_tb;

architecture Behavioral of ASCEN_tb is

COMPONENT ASCEN 
    Port ( clock : in STD_LOGIC;
           RST: in STD_LOGIC;
           planta_destino : in STD_LOGIC_VECTOR (2 DOWNTO 0);
           planta_actual : in STD_LOGIC_VECTOR (2 DOWNTO 0);
           LED_P1: out STD_LOGIC;
           LED0: out STD_LOGIC;
           DIS_DEST: out STD_LOGIC_VECTOR (2 DOWNTO 0);
           DIS_ACT: out STD_LOGIC_VECTOR (2 DOWNTO 0);
           motor_out : out STD_LOGIC_VECTOR (1 downto 0)
           );               
END COMPONENT;

constant K: time := 100 ns;
SIGNAL clk: STD_LOGIC:='0';
SIGNAL rst: STD_LOGIC:='0';
SIGNAL planta_destino: STD_LOGIC_VECTOR (2 DOWNTO 0):= "000";
SIGNAL motor_out: STD_LOGIC_VECTOR (1 DOWNTO 0);
SIGNAL planta_actual: STD_LOGIC_VECTOR (2 DOWNTO 0);
SIGNAL LED_P1: STD_LOGIC:='0';
SIGNAL LED0: STD_LOGIC:='0';
SIGNAL planta_d: STD_LOGIC_VECTOR (2 DOWNTO 0):= "000";
SIGNAL planta_a: STD_LOGIC_VECTOR (2 DOWNTO 0);


begin

ascensor: ASCEN
    PORT MAP(
        clock => clk,
        RST => rst,
        planta_destino => planta_destino,
        motor_out => motor_out,
        planta_actual => planta_actual,
        DIS_DEST => planta_d,
        DIS_ACT => planta_a,
        LED_P1 => LED_P1,
        LED0 => LED0
        );
        
clk <= not clk after 0.5 * K; 
planta_destino <= "011" after 0.4 * K, "000" after 8.5 * K;
planta_actual <= "010" after 0.4 * K, "000" after 8.5 * K;
rst <= '1' after 4 * K;

end Behavioral;
