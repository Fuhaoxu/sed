
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity PUERTTA_tb is

end PUERTTA_tb;

architecture Behavioral of PUERTTA_tb is

constant K: time := 100 ns;
SIGNAL clk: STD_LOGIC:='0';
SIGNAL ORDEN: STD_LOGIC:='0';
SIGNAL RESUL: STD_LOGIC:='0';
SIGNAL LEDD0: STD_LOGIC:='0';

COMPONENT PUERTTA 
    Port ( clock: in STD_LOGIC;
           orden : in STD_LOGIC;
           resultado : out STD_LOGIC;
           LED0 : out STD_LOGIC);
end COMPONENT;

begin

puerta: PUERTTA
    PORT MAP(
        clock => clk,
        orden => ORDEN,
        resultado => RESUL,
        LED0 => LEDD0
        );

clk <= not clk after 0.5 * K;   
ORDEN <= '1' after 0.7 * K, not ORDEN after 4 * K;

end Behavioral;
