
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity top_tb is

end top_tb;


architecture Behavioral of top_tb is

COMPONENT TOP 
    Port ( clk : in STD_LOGIC;
           PD1 : in STD_LOGIC;
           PD2 : in STD_LOGIC;
           PD3 : in STD_LOGIC;
           PD4 : in STD_LOGIC;
           PA1 : in STD_LOGIC;
           PA2 : in STD_LOGIC;
           PA3 : in STD_LOGIC;
           PA4 : in STD_LOGIC;
           reset : in STD_LOGIC;
           motor_LED1 : out STD_LOGIC;
           motor_LED2 : out STD_LOGIC;
           LED_INDICADOR_LLEGADDA: out STD_LOGIC;
           puerta : out STD_LOGIC;
           DISPLAY_SELEC : out STD_LOGIC_VECTOR (7 downto 0);
           DISPLAY_NUMER : out STD_LOGIC_VECTOR (6 downto 0));
end COMPONENT;

constant K: time := 100 ns;
SIGNAL clock:std_logic:='0';
SIGNAL le1,le2,pue:std_logic;
SIGNAL pd1:std_logic:='0';
SIGNAL pd2:std_logic:='0';
SIGNAL pd3:std_logic:='0';
SIGNAL pd4:std_logic:='0';
SIGNAL pa1:std_logic:='0';
SIGNAL pa2:std_logic:='0';
SIGNAL pa3:std_logic:='0';
SIGNAL pa4:std_logic:='0';
SIGNAL re:std_logic:='0';
SIGNAL d_s: STD_LOGIC_VECTOR (7 downto 0);
SIGNAL d_n: STD_LOGIC_VECTOR (6 downto 0);
SIGNAL LED_LLEGADA: STD_LOGIC;
begin

fd: TOP
    PORT MAP(
        clk => clock,
        PD1 => pd1,
        PD2 => pd2,
        PD3 => pd3,
        PD4 => pd4,
        PA1 => pa1,
        PA2 => pa2,
        PA3 => pa3,
        PA4 => pa4,
        reset => re,
        motor_LED1 => le1,
        motor_LED2 => le2,
        LED_INDICADOR_LLEGADDA => LED_LLEGADA,
        puerta => pue,
        DISPLAY_SELEC => d_s,
        DISPLAY_NUMER => d_n
        );
        
clock <= not clock after 0.5 * K;

pd2 <= '1' after 1 * K, not pd2 after 4 * K;
pa3 <= '1' after 1 * K, not pa3 after 4 * K;
--p4 <= '1' after 2.5 * K, '0' after 3.5 * K;

end Behavioral;
