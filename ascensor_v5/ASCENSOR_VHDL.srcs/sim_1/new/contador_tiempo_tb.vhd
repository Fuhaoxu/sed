
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity contador_tiempo_tb is

end contador_tiempo_tb;

architecture Behavioral of contador_tiempo_tb is

COMPONENT contador_tiempo 
    GENERIC (UNO: INTEGER:=1;
             DOS: INTEGER:=2;
             TRES: INTEGER:=3
             );
    Port ( clock_1Hz : in STD_LOGIC;
           start1: in STD_LOGIC;
           start2: in STD_LOGIC;
           start3: in STD_LOGIC;
           tiempo_1s : out STD_LOGIC;
           tiempo_2s : out STD_LOGIC;
           tiempo_3s : out STD_LOGIC);
END COMPONENT;

constant K: time := 1000 ns;
SIGNAL clk_1Hz: STD_LOGIC:='0';
SIGNAL st1: STD_LOGIC:='0';
SIGNAL st2: STD_LOGIC:='0';
SIGNAL st3: STD_LOGIC:='0';
SIGNAL t1s: STD_LOGIC;
SIGNAL t2s: STD_LOGIC;
SIGNAL t3s: STD_LOGIC;

begin

ctt: contador_tiempo
    PORT MAP(
        clock_1Hz => clk_1Hz,
        start1 => st1,
        start2 => st2,
        start3 => st3,
        tiempo_1s => t1s,
        tiempo_2s => t2s,
        tiempo_3s => t3s
    );

clk_1Hz <= not clk_1Hz after 0.5 * K;
st1 <= '1' after 0.4 * K, not st1 after 2 * K;
st2 <= '1' after 0.4 * K, not st2 after 2 * K;
st3 <= '1' after 0.4 * K, not st3 after 2 * K;

end Behavioral;
