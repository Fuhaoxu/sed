

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity TOP is
    Port ( clk : in STD_LOGIC;
           P1 : in STD_LOGIC;
           P2 : in STD_LOGIC;
           P3 : in STD_LOGIC;
           P4 : in STD_LOGIC;
           motor_LED1 : out STD_LOGIC;
           motor_LED2 : out STD_LOGIC;
           puerta : out STD_LOGIC;
           DISPLAY1 : out STD_LOGIC_VECTOR (6 downto 0);
           DISPLAY2 : out STD_LOGIC_VECTOR (6 downto 0));
end TOP;

ARCHITECTURE ASCENSOR OF TOP IS

COMPONENT DEC1 
	PORT(
	clock, boton1, boton2, boton3, boton4: IN STD_LOGIC;
	planta_destino1: OUT INTEGER
);
		END COMPONENT;

COMPONENT DEC2
	PORT(
	clock: IN STD_LOGIC;
	planta_actual2: IN INTEGER;
	pines_display2: OUT STD_LOGIC_VECTOR (6 DOWNTO 0)
);
		END COMPONENT;

COMPONENT DEC3
	PORT(
	clock: IN STD_LOGIC;
	planta_destino3: IN INTEGER ;
	pines_display1: OUT STD_LOGIC_VECTOR (6 DOWNTO 0)
);
		END COMPONENT;

COMPONENT PUERTTA 
	PORT(
	clock: IN STD_LOGIC;
	orden: IN STD_LOGIC;
	resultado: OUT STD_LOGIC;
	LED0: OUT STD_LOGIC
);
		END COMPONENT;

COMPONENT MOTOR 
	PORT(
	clock: IN STD_LOGIC;
	SENSOR_MOTOR: IN STD_LOGIC_VECTOR (1 DOWNTO 0);
	LED1: OUT STD_LOGIC;
    LED2: OUT STD_LOGIC
);
		END COMPONENT;

COMPONENT ASCEN
	PORT(
	planta_destino: IN INTEGER;
    clock: IN STD_LOGIC;
    enable: IN STD_LOGIC;
    motor_out: OUT STD_LOGIC_VECTOR (1 DOWNTO 0);
    planta_act_sal: OUT INTEGER;
    puerta_ordenn: OUT STD_LOGIC
);
		END COMPONENT;

------------------------------------ DECLARACION DE SE?ALES
SIGNAL P_DEST: INTEGER;
SIGNAL PUERTA_ORDEN: STD_LOGIC;
SIGNAL ESTADO_PUERTA: STD_LOGIC;
SIGNAL ESTADO_MOTOR: STD_LOGIC_VECTOR (1 DOWNTO 0);
SIGNAL PLANTA_ACTUAL: INTEGER := 1;
TYPE state_type IS (S1, S2, S3);
SIGNAL state, next_state: state_type;


BEGIN


Decoder1: DEC1
	PORT MAP( 
	clock => clk,
	boton1 => P1,
	boton2 => P2,
	boton3 => P3,
    boton4 => P4,
    planta_destino1 => P_DEST  );


Decoder3: DEC3
	PORT MAP(
	clock => clk,
	planta_destino3 => P_DEST,
	pines_display1 => DISPLAY1
);


Puertaa: PUERTTA 
	PORT MAP(	
	clock => clk,
	orden => PUERTA_ORDEN,
	resultado => ESTADO_PUERTA,
    LED0 => puerta
);


Mottor: MOTOR
	PORT MAP(
	clock => clk,
	SENSOR_MOTOR => ESTADO_MOTOR,
	LED1 => motor_LED1,
	LED2 => motor_LED2
);


Decoder2: DEC2
	PORT MAP(
	clock => clk,
	pines_display2 => DISPLAY2,
	planta_actual2 => PLANTA_ACTUAL
);


ascensorr: ASCEN
	PORT MAP(
	clock => clk,
	enable => ESTADO_PUERTA,
	motor_out => ESTADO_MOTOR,
	puerta_ordenn => PUERTA_ORDEN,
	planta_act_sal => PLANTA_ACTUAL,
	planta_destino => P_DEST
);

-----------------------------------------------------------------

DECODIFICADOR1: PROCESS (clk, P1, P2, P3, P4)
	BEGIN
		IF rising_edge (clk) THEN
		   P_DEST <= 1 WHEN P1 = '1' ELSE
					2 WHEN P2 = '1' ELSE
					3 WHEN P3 = '1' ELSE
					4 WHEN P4 = '1' ;
		END IF;
END PROCESS; 
	 


DECODIFICADOR2: PROCESS (clk, PLANTA_ACTUAL)
	BEGIN
		IF rising_edge (clk) THEN
    DISPLAY2 <= "0110000" WHEN PLANTA_ACTUAL = 1 ELSE
				"1101101" WHEN PLANTA_ACTUAL = 2 ELSE
				"1111000" WHEN PLANTA_ACTUAL = 3 ELSE
				"0110011" WHEN PLANTA_ACTUAL = 4;
			END IF;
      END PROCESS;

	DECODIFICADOR3: PROCESS (clk, P_DEST)
		BEGIN
		   IF rising_edge (clk) THEN
				DISPLAY1 <= "0110000" WHEN P_DEST = 1 ELSE
								"1101101" WHEN P_DEST = 2 ELSE
								"1111000" WHEN P_DEST = 3 ELSE
								"0110011" WHEN P_DEST = 4 ;
			END IF;       END PROCESS;

motorrr: PROCESS (clk, ESTADO_MOTOR)
		BEGIN
    IF rising_edge (clk) THEN
          motor_LED1 <= '0' WHEN ESTADO_MOTOR = "00" ELSE
				 '0' WHEN ESTADO_MOTOR = "01" ELSE
				'1' WHEN ESTADO_MOTOR = "10" ;
		  motor_LED2 <= '0' WHEN ESTADO_MOTOR = "00" ELSE
				 '1' WHEN ESTADO_MOTOR = "01" ELSE
				'0' WHEN ESTADO_MOTOR = "10" ;
	END IF;
END PROCESS;
				

puerta <= ESTADO_PUERTA;

puertaaa: PROCESS --(clk, PUERTA_ORDEN)
BEGIN
--IF rising_edge (clk) THEN
		IF PUERTA_ORDEN = '1' THEN
            WAIT FOR 2ns;
            ESTADO_PUERTA <= '1';
        ELSIF PUERTA_ORDEN = '0' THEN
            WAIT FOR 2ns;
            ESTADO_PUERTA <= '0';
        END IF;   
--END IF;
END PROCESS;



SINCRONIZACION_RELOJ: PROCESS (clk)
	BEGIN
		IF rising_edge (clk) THEN
	state <= next_state;
END IF;
END PROCESS;


TRATAMIENTO_PUERTA: PROCESS(clk,P_DEST)
BEGIN
--CASE (state) IS
--	WHEN S1 =>
    IF state=S1 THEN
	IF rising_edge (clk) THEN
	IF (P_DEST=1 OR P_DEST=2 OR P_DEST=3 OR P_DEST=4) THEN
    PUERTA_ORDEN <= '0';
END IF; 
END IF;
END IF;
--END CASE;
END PROCESS;



NEXT_STATE_DECODE: PROCESS ---------(PLANTA_ACTUAL, ESTADO_PUERTA, P_DEST)
  BEGIN
		next_state <= S1;

	IF (state=S1) THEN
IF (P_DEST-PLANTA_ACTUAL=1 AND ESTADO_PUERTA = '0') THEN
				next_state <= S2;
			    WAIT FOR 3ns;
				PLANTA_ACTUAL <= P_DEST;
				next_state <= S1;
				PUERTA_ORDEN <= '1';
ELSIF (P_DEST-PLANTA_ACTUAL=2 AND ESTADO_PUERTA = '0') THEN
				next_state <= S2;
				WAIT FOR 3ns;
				PLANTA_ACTUAL <= PLANTA_ACTUAL+1;
				WAIT FOR 1ns;
				PLANTA_ACTUAL <= P_DEST;
				next_state <= S1;
				PUERTA_ORDEN <= '1';
ELSIF (P_DEST-PLANTA_ACTUAL=3 AND ESTADO_PUERTA = '0') THEN
				next_state <= S2;
				WAIT FOR 3ns;
				PLANTA_ACTUAL <= PLANTA_ACTUAL+1;
				WAIT FOR 1ns;
				PLANTA_ACTUAL <= PLANTA_ACTUAL+1;
                WAIT FOR 1ns;
                PLANTA_ACTUAL <= P_DEST;
				next_state <= S1;
				PUERTA_ORDEN <= '1';
ELSIF (PLANTA_ACTUAL-P_DEST=1 AND ESTADO_PUERTA = '0') THEN
				next_state <= S3;
			    WAIT FOR 3ns;
				PLANTA_ACTUAL <= P_DEST;
				next_state <= S1;
				PUERTA_ORDEN <= '1';
ELSIF (PLANTA_ACTUAL-P_DEST=2 AND ESTADO_PUERTA = '0') THEN
				next_state <= S3;
				WAIT FOR 3ns;
				PLANTA_ACTUAL <= PLANTA_ACTUAL-1;
				WAIT FOR 1ns;
                PLANTA_ACTUAL <= P_DEST;
				next_state <= S1;
				PUERTA_ORDEN <= '1';
ELSIF (PLANTA_ACTUAL-P_DEST=3 AND ESTADO_PUERTA = '0') THEN
				next_state <= S3;
				WAIT FOR 3ns;
				PLANTA_ACTUAL <= PLANTA_ACTUAL-1;
				WAIT FOR 1ns;
				PLANTA_ACTUAL <= PLANTA_ACTUAL-1;
                WAIT FOR 1ns;
                PLANTA_ACTUAL <= P_DEST;
				next_state <= S1;
				PUERTA_ORDEN <= '1';
END IF;
END IF;
END PROCESS;



Estado_del_motor: PROCESS (state)
	BEGIN
ESTADO_MOTOR <= "00" WHEN state=S1 ELSE
                "01" WHEN state=S2 ELSE
                "10" WHEN state=S3 ;
	END PROCESS;


END ARCHITECTURE;

