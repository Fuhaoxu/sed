
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity Display_Refresh_tb is

end Display_Refresh_tb;

architecture Behavioral of Display_Refresh_tb is

COMPONENT Display_refresh 
    Port ( clock : in STD_LOGIC;
           display_selection : out STD_LOGIC_VECTOR (7 downto 0);
           display_number : out STD_LOGIC_VECTOR (6 downto 0);
           planta_actuall : in STD_LOGIC_VECTOR (6 downto 0);
           planta_destinoo : in STD_LOGIC_VECTOR (6 downto 0));
END COMPONENT;

constant K: time := 100 ns;
SIGNAL clk: STD_LOGIC:='0';
SIGNAL D_S: STD_LOGIC_VECTOR (7 DOWNTO 0);
SIGNAL D_N, P_A, P_D: STD_LOGIC_VECTOR (6 DOWNTO 0);

begin

D_R: Display_refresh
    PORT MAP(
        clock => clk,
        display_selection => D_S,
        display_number => D_N,
        planta_actuall => P_A,
        planta_destinoo => P_D
        );

clk <= not clk after 0.5 * K; 
P_A <= "1001111";
P_D <= "1001100";

end Behavioral;
