
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity contador_tiempo is
    GENERIC (TRES: INTEGER:=3;
             CUATRO: INTEGER:=4;
             CINCO: INTEGER:=5
             );
    Port ( clock_1Hz : in STD_LOGIC;
           start4: in STD_LOGIC;
           start5: in STD_LOGIC;
           start3: in STD_LOGIC;
           tiempo_4s : out STD_LOGIC;
           tiempo_5s : out STD_LOGIC;
           tiempo_3s : out STD_LOGIC);
end contador_tiempo;

architecture Behavioral of contador_tiempo is
SIGNAL CINCOSEC: STD_LOGIC:='0';
SIGNAL CUATROSEC: STD_LOGIC:='0';
SIGNAL TRESSEC: STD_LOGIC:='0';
begin

SEC5: process (clock_1Hz, start5)
  variable cnt1:integer:=0;
  begin
  
		  if clock_1Hz'event and clock_1Hz='1' then
		      if start5 = '1' then 
			     if (cnt1=CINCO) then
				CINCOSEC <= '1';
				cnt1:=0;
			     ELSE
			    CINCOSEC <= '0';
				cnt1:=cnt1+1;
		end if;
		end if;
		end if;
  end process;
 tiempo_5s <= CINCOSEC;
 
 
 SEC4: process (clock_1Hz, start4)
  variable cnt2:integer:=0;
  begin
  
	if clock_1Hz'event and clock_1Hz='1' then
		  if start4 = '1' then 
			if (cnt2=CUATRO) then
				CUATROSEC <= '1';
				cnt2:=0;
			ELSE
			    CUATROSEC <= '0';
				cnt2:=cnt2+1;
		end if;
		end if;
		end if;
  end process;
 tiempo_4s <= CUATROSEC;
 
 SEC3: process (clock_1Hz, start3)
  variable cnt3:integer:=0;
  begin
    
		  if clock_1Hz'event and clock_1Hz='1' then
		       if start3 = '1' then 
			     if (cnt3=TRES) then			
				TRESSEC <= '1';
				cnt3:=0;
			     ELSE
			    TRESSEC <= '0';
				cnt3:=cnt3+1;
		end if;
		end if;
		end if;
  end process;
 tiempo_3s <= TRESSEC;
 

end Behavioral;
