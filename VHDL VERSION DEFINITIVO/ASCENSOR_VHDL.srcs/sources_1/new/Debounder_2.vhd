
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity Debouncer_2 is
    Port ( clock : in STD_LOGIC;
           c : in STD_LOGIC;
           Pc_OUT: out STD_LOGIC);
end Debouncer_2;

architecture Behavioral of Debouncer_2 is
signal R11, R12, R13: std_logic;
begin

process(clock)
    BEGIN
        if (clock'event and clock = '1') then

            R11 <= c;
            R12 <= R11;
            R13 <= R12;
            -- BOTON c
            
             end if;
    end process;
    Pc_OUT <= R11 and R12 and (not R13);

end Behavioral;
