
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity DEC1 is
    Port ( clock : in STD_LOGIC;
           boton1 : in STD_LOGIC;
           boton2 : in STD_LOGIC;
           boton3 : in STD_LOGIC;
           boton4 : in STD_LOGIC;
           planta_destino1 : out STD_LOGIC_VECTOR (2 DOWNTO 0));
end DEC1;

architecture Behavioral of DEC1 is

begin

DECODIFICADOR1: PROCESS (clock, boton1, boton2, boton3, boton4)
	BEGIN
		IF rising_edge (clock) THEN
		  planta_destino1 <= "001" WHEN boton1 = '1' ELSE
					         "010" WHEN boton2 = '1' ELSE
				             "011" WHEN boton3 = '1' ELSE
					         "100" WHEN boton4 = '1' ;
		END IF;     
END PROCESS; 

end Behavioral;
